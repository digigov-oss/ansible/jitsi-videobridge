# Ansible jitsi-videobridge role

## Overview

This role installs and configures the videobridge part of jitsi. For
more information, see the documentation of the `jitsi-meet` role.

## Meta

Written by Antonis Christofides. May contain some stuff from
https://github.com/udima-university/ansible-jitsi-meet.

Copyright (C) 2022-2023 GRNET  
Copyright (C) 2020-2022 The copyright holders of ansible-jitsi-meet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
